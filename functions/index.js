'use strict';
const functions = require('firebase-functions');
const admin = require('firebase-admin');
// const moment = require('moment');
// const request = require('request');
const serviceAccount = require('./debt-rearrangement-key.json');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
// const { user } = require('firebase-functions/lib/providers/auth');

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Initialize DB
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://debt-rearrangement-40fb8.firebaseio.com",
    databaseAuthVariableOverride: null
});

//Database and root reference
var db = admin.database();
var userRef = db.ref('users_resources');

// app.get('/getUsers', async (req, res) => {
//     // eslint-disable-next-line promise/always-return
//     userRef.once('value').then(snapshot => {
//         let users = [];
//         users.push(snapshot.val());
//         return res.send(users);
//     });
// })

// app.get('/getUsers/:id', async (req, res) => {
//     // var myUserId = admin.auth().getUser(req.params.id)
//     userRef.child(req.params.id + '/debt_items')
//     .once('value')
//     .then(snapshot => {
//         let debtId = []
//         debtId.push(snapshot.val())
//         console.log(snapshot.val())
//         var ov = snapshot.val();
//         for (let k in ov) {
//             console.log(ov[k].rateType)
//             ov[k].rateType
//         }
//         return res.send(debtId)
//     });
// });

// app.post('/getUsers', async (req, res) => {
//     // var myUserId = admin.auth().getUser(req.params.id)
//     let x = req.body.x;
//     let y = req.body.y;
//     let k = x+y
//     console.log("show req", req.body.x);
//     console.log( "show x ", x);
//     console.log("show y ", y);
//     console.log("test ", k);
    
//   });

app.post('/onEffectiveRate', async (req, res) => {
    let uid = req.body.uid;
    let debtId = req.body.debtId;

    var apr, bank, category, debtName, monthlyPayment, paymentDueDate, rateType, startDate, totalInstallment, totalLoan;
    var principleLeft, currentPrinciple, principle;
    var interest, totalInterest;
    var daysInYears = 365;

    var generalInformation = {};
    var paymentInformation = [];
    var overallInformation = [];

    // eslint-disable-next-line promise/catch-or-return
    userRef.child(uid + '/debt_items/' + debtId)
    .once('value')
    .then((snapshot) => {
        debtName = snapshot.val().debtName
        category = snapshot.val().category
        bank = snapshot.val().bank
        startDate = snapshot.val().startDate
        totalLoan = parseFloat(snapshot.val().totalLoan).toFixed(4) 
        console.log(typeof generalInformation)
        // generalInformation.debtName = debtName
        // generalInformation.push(debtName,category,bank,startDate,totalLoan)

        // apr = parseFloat(snapshot.val().apr).toFixed(4)
        // interest =  parseFloat((totalLoan*apr*30)/365).toFixed(2)

        console.log(JSON.stringify({debtName}))
        return res.send(JSON.stringify({debtName}))
        // if (snapshot.val().rateType === "Effective Rate") {

        // } else {
        //     console.log(false)
        //     return res.send(false)
        // }
    });
}); 

// app.post('/onEffectiveCalculated', async (req, res) => {
//     let uid = req.body.uid;
//     let debtId = req.body.debtId;
    
//     userRef.child(uid + '/debt_items/' + debtId)
//     .once('value')
//     // eslint-disable-next-line promise/always-return
//     .then((snapshot) => {
//         // let debtId = [];
//         // debtId.push(snapshot.val());
//         // console.log(debtId)
//         let result = [];
//         var debtData = snapshot.val();
//         console.log(snapshot.val())
//         // eslint-disable-next-line promise/always-return
//         // for(let k in debtData) {
//         //     // console.log(debtData[k].monthlyPayment)
//         //     var monthlyPayment = parseFloat(debtData[k].monthlyPayment).toFixed(2)
//         //     var totalInstallment = parseFloat(debtData[k].totalInstallment).toFixed(2)
//         //     var total = monthlyPayment*totalInstallment
//         //     result.push(total)
//         //     console.log(result)
            
//         // }
//         // return res.status(200)
//         // var x = 1,  y=2, z= "xxx";
//         // return res.json({"a":x, "b":y,"z":z})
//     });
    
// });

app.get('/', (req, res) => res.send("Hello!"));

exports.calculator = functions.https.onRequest(app);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
